﻿#include <iostream>
#include <string>
using std::cin;
using std::cout;
using std::endl;

struct PlayerScore
{
private:

    int count=0;
    std::string name;
    int score=0;
        
public:

    void sortNames (PlayerScore* count, int length)
    {
        
        for (int startIndex = 0; startIndex < length; ++startIndex)
        {
            int largestIndex = startIndex;

            for (int currentIndex = startIndex + 1; currentIndex < length; ++currentIndex)
            {
                if (count[currentIndex].score > count[largestIndex].score)
                largestIndex = currentIndex;
            }
            std::swap(count[startIndex], count[largestIndex]);
        }
    }

    int p_count()
    {
        setlocale(0, "Russian");
        int m_count = 0;
         do
         {
           cout << "Сколько у вас игроков? ";
           cin >> m_count;
           cout << endl;
         } while (m_count <= 1);

         PlayerScore* count = new PlayerScore[m_count];

         for (int i = 0; i < m_count; ++i)
         {
             cout << "Введите имя игрока " << i + 1 << ": ";
             cin >> count[i].name;
             cout << endl;
             cout << "Введите количество очков игрока " << i + 1 << ": ";
             cin >> count[i].score;
             cout << endl;
         }
         
         sortNames(count, m_count);
        
         for (int index = 0; index < m_count; ++index)
             cout << count[index].name << " набрал " << count[index].score << endl <<endl;

         delete[] count;

         return 0;
    }
};

int main()
{
    PlayerScore A;
    A.p_count();
}

